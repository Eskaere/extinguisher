// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "FlameThrowerGameMode.generated.h"

UCLASS(minimalapi)
class AFlameThrowerGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AFlameThrowerGameMode();
};



